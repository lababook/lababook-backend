module.exports = {
  USER_NOT_FOUND: 'pengguna tidak ditemukan',
  WRONG_CREDENTIAL: 'telepon atau password salah',
  NOT_AUTHENTICATED: 'silahkan login terlebih dahulu',
  NOT_ACTIVE: 'silahkan aktivasi nomor handphone',
  NOT_AUTHORIZED: 'anda tidak memiliki akses ke fitur ini',
  WRONG_OTP: 'kode otp yang anda masukkan salah',
  OTP_EXPIRED: 'kode otp sudah tidak berlaku',
  OTP_STILL_ACTIVE: 'harap tunggu beberapa saat lagi'
}
