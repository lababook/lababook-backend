const {
  TWILIO_SID,
  TWILIO_TOKEN,
  TWILIO_NUMBER
} = process.env
const client = require('twilio')(TWILIO_SID, TWILIO_TOKEN)

const { formatPhone } = require('../phone')

const sendOtp = (otp, recipient) => {
  return client.messages
    .create({
      body: 'kode anda adalah ' + otp,
      from: TWILIO_NUMBER,
      to: formatPhone(recipient)
    })
}

module.exports = {
  sendOtp
}
