const createError = require('http-errors')

const errMsg = require('../../constants/errorMessages')

const activeOnly = async (req, _, next) => {
  try {
    if (!req.user.isActive) {
      throw createError(403, errMsg.NOT_ACTIVE)
    }

    return next()
  } catch (error) {
    return next(error)
  }
}

module.exports = {
  activeOnly
}
