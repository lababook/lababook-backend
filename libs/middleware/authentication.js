const createError = require('http-errors')

const { decodeToken } = require('../auth')
const errMsg = require('../../constants/errorMessages')

module.exports = async (req, _, next) => {
  try {
    if (!req.headers.Authorization && !req.headers.authorization) {
      throw createError(403, errMsg.NOT_AUTHENTICATED)
    }

    const token = req.headers.Authorization || req.headers.authorization
    const headers = await decodeToken(token)

    req.user = headers

    return next()
  } catch (error) {
    return next(error)
  }
}
