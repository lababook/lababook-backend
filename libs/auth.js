const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const ms = require('ms')

const { JWT_SECRET, SALT_ROUND } = process.env

const generateHash = text => {
  return new Promise((resolve, reject) => {
    bcrypt.genSalt(parseInt(SALT_ROUND), (err, salt) => {
      if (err) return reject(err)

      bcrypt.hash(text, salt, (err, hash) => {
        if (err) return reject(err)

        return resolve(hash)
      })
    })
  })
}

const compareHash = (text, hash) => {
  return new Promise((resolve, reject) => {
    bcrypt.compare(text, hash, (err, res) => {
      if (err) return reject(err)

      resolve(res)
    })
  })
}

const signToken = data => {
  return new Promise((resolve, reject) => {
    jwt.sign(
      data,
      JWT_SECRET,
      {
        expiresIn: ms('7d')
      },
      (err, token) => {
        if (err) return reject(err)

        return resolve(token)
      }
    )
  })
}

const decodeToken = token => {
  return new Promise((resolve, reject) => {
    jwt.verify(
      token,
      JWT_SECRET,
      (err, decoded) => {
        if (err) return reject(err)

        return resolve(decoded)
      }
    )
  })
}

module.exports = {
  generateHash,
  compareHash,
  signToken,
  decodeToken
}
