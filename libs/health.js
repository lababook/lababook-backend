const health = (_, res, next) => {
  const information = {
    status: 'OK',
    environment: process.env.NODE_ENV,
    pid: process.pid,
    cpu: process.cpuUsage(),
    memory: process.memoryUsage(),
    uptime: process.uptime()
  }

  res.json(information)
}

module.exports = {
  health
}
