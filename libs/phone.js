const formatPhone = (phoneNumber, countryCode = '+62') => {
  const clean = phoneNumber.replace(/^0/i, '')

  return countryCode + clean
}

module.exports = {
  formatPhone
}
