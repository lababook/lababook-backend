const operatorPrefixes = ['811', '812', '813', '821', '822', '852', '853', '823', '851', '811', '812', '813', '821', '822', '852', '853', '823', '851', '817', '818', '819', '859', '877', '878', '838', '831', '832', '833', '895', '896', '897', '898', '899', '881', '882', '883', '884', '885', '886', '887', '888', '889']

class PhoneFormatErr extends Error {
  constructor (message) {
    super()
    this.message = message
    this.isJoi = true
  }
}

const isMobileValid = (phoneNumber = '') => {
  const normalized = phoneNumber.replace(/^0|^\+\w{2}/, '')
  const prefix = normalized.slice(0, 3)

  if (operatorPrefixes.indexOf(prefix) === -1 || normalized.length < 9) {
    throw new PhoneFormatErr('Nomor handphone tidak valid')
  }

  return true
}

module.exports = {
  operatorPrefixes,
  isMobileValid
}
