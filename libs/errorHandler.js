const { NODE_ENV } = process.env

const generalHandler = (error, req, res, next) => {
  if (NODE_ENV !== 'test') {
    console.log(error)
  }

  let errorCode = (error.status) || 500

  if (error.isJoi) {
    errorCode = 400
  }

  res
    .status(errorCode)
    .json({
      message: error.message,
      ...(error.isJoi && {
        isJoi: true
      }),
      ...(NODE_ENV !== 'test' && {
        stack: error.stack
      })
    })
}

module.exports = {
  generalHandler
}
