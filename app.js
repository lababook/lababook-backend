require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const helmet = require('helmet')
const logger = require('morgan')

const db = require('./models')
const routes = require('./routes')
const { health } = require('./libs/health')
const { generalHandler: errHandler } = require('./libs/errorHandler')

const app = express()

db.sequelize
  .authenticate()
  .then(() => {
    console.log('connected to db')
  })
  .catch(err => {
    console.error('cannot connect to db', err)
  })

app.use(cors())
app.use(helmet())
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/health', health)
app.use(routes)
app.use(errHandler)

module.exports = app
