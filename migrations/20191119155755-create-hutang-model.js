module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'Hutang',
      {
        id: {
          type: Sequelize.UUID,
          defaulValue: Sequelize.UUIDV4,
          primaryKey: true
        },
        userId: {
          type: Sequelize.INTEGER,
          references: {
            model: {
              tableName: 'Users'
            },
            key: 'id'
          }
        },
        contactId: {
          type: Sequelize.UUID,
          references: {
            model: {
              tableName: 'Contacts'
            },
            key: 'id'
          }
        },
        delta: {
          type: Sequelize.BIGINT,
          allowNull: false
        },
        trxType: {
          type: Sequelize.STRING,
          enum: ['credit', 'debit'],
          allowNull: false
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
        deletedAt: Sequelize.DATE
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Hutang')
  }
}
