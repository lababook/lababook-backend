module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'Contacts',
      {
        id: {
          type: Sequelize.UUID,
          primaryKey: true,
          defaultValue: Sequelize.UUIDV4
        },
        userId: {
          type: Sequelize.INTEGER,
          references: {
            model: {
              tableName: 'Users'
            },
            key: 'id'
          }
        },
        name: {
          type: Sequelize.STRING,
          required: true
        },
        countryCode: {
          type: Sequelize.STRING,
          defaultValue: '+62'
        },
        phoneNumber: {
          type: Sequelize.STRING,
          unique: true
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
        deletedAt: Sequelize.DATE
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Contacts')
  }
}
