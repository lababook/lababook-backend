'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'Users',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        name: {
          type: Sequelize.STRING
        },
        bookName: {
          type: Sequelize.STRING,
          required: true
        },
        countryCode: {
          type: Sequelize.STRING,
          defaultValue: '+62'
        },
        phoneNumber: {
          type: Sequelize.STRING,
          unique: true
        },
        imageUrl: {
          type: Sequelize.STRING
        },
        isActive: {
          type: Sequelize.BOOLEAN,
          defaultValue: false
        },
        password: Sequelize.STRING,
        otp: Sequelize.STRING,
        otpExpiredAt: Sequelize.DATE,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
        deletedAt: Sequelize.DATE
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users')
  }
}
