const Joi = require('@hapi/joi')
const createError = require('http-errors')
const ms = require('ms')

const errMsg = require('../constants/errorMessages')
const { signToken } = require('../libs/auth')
const { User } = require('../models')
const { generateOtp } = require('../libs/otp')
const { sendOtp } = require('../libs/notification/smsOtp')
const { isMobileValid } = require('../libs/validator/phoneNumber')

const { SEND_OTP = 'false', OTP_AGE } = process.env

const validateUser = async body => {
  const schema = Joi.object().keys({
    name: Joi.string()
      .trim()
      .empty(''),
    imageUrl: Joi.string()
      .trim()
      .uri()
      .empty(''),
    phoneNumber: Joi.string()
      .trim()
      .required(),
    bookName: Joi.string()
      .trim()
      .required()
  })
  const data = await schema.validateAsync(body)

  if (isMobileValid(data.phoneNumber)) {
    data.phoneNumber = data.phoneNumber.replace(/^0/i, '')
  }

  return data
}

const generateToken = async user =>
  signToken({
    userId: user.id,
    phoneNumber: user.phoneNumber,
    countryCode: user.countryCode,
    isActive: user.isActive
  })

const login = async (req, res, next) => {
  try {
    const schemaBody = Joi.object().keys({
      phoneNumber: Joi.string()
        .trim()
        .empty()
        .required()
        .messages({
          'string.empty': 'Nomor handphone tidak boleh kosong',
          'any.required': 'Harap masukkan nomor handphone Anda'
        })
    })
    const data = await schemaBody.validateAsync(req.body)

    if (isMobileValid(data.phoneNumber)) {
      data.phoneNumber = data.phoneNumber.replace(/^0/i, '')
    }

    let user = await User.findOne({
      where: {
        phoneNumber: data.phoneNumber
      }
    })

    if (!user) {
      user = await User.create({
        phoneNumber: data.phoneNumber
      })
    }

    const otp = generateOtp()

    if (SEND_OTP.toLowerCase() === 'true') {
      await sendOtp(otp, user.phoneNumber)
    }

    user.otp = otp
    user.otpExpiredAt = Date.now() + ms(OTP_AGE)

    await user.save()

    return res.json({
      status: 'ok',
      user: {
        phoneNumber: user.phoneNumber
      }
    })
  } catch (error) {
    return next(error)
  }
}

const checkOtp = async (req, res, next) => {
  try {
    const data = await Joi.object().keys({
      phoneNumber: Joi.string()
        .trim()
        .required(),
      otp: Joi.string()
        .trim()
        .required()
    }).validateAsync(req.body)

    const user = await User.findOne({
      where: {
        phoneNumber: data.phoneNumber,
        otp: data.otp
      }
    })

    if (!user) throw createError(400, errMsg.WRONG_OTP)

    if (user.otpExpiredAt.getTime() < Date.now()) {
      throw createError(400, errMsg.OTP_EXPIRED)
    }

    let isNew = false
    if (!user.isActive) {
      isNew = true
      user.isActive = true

      await user.save()
    }

    const token = await generateToken(user)

    return res.json({
      status: 'ok',
      isNew,
      token,
      user: {
        id: user.id,
        name: user.name,
        phoneNumber: user.phoneNumber,
        countryCode: user.countryCode,
        imageUrl: user.imageUrl
      }
    })
  } catch (error) {
    return next(error)
  }
}

const resendOtp = async (req, res, next) => {
  try {
    const data = await Joi.object().keys({
      phoneNumber: Joi.string()
        .trim()
        .required()
    }).validateAsync(req.body)

    const user = await User.findOne({
      where: {
        phoneNumber: data.phoneNumber
      }
    })

    if (!user) throw new Error('auth.resendOtp - USER NOT FOUND')

    if (user.otpExpiredAt.getTime() >= Date.now()) {
      throw createError(400, errMsg.OTP_STILL_ACTIVE)
    }

    const otp = generateOtp()

    if (SEND_OTP.toLowerCase() === 'true') {
      await sendOtp(otp, user.phoneNumber)
    }

    user.otp = otp
    user.otpExpiredAt = Date.now() + ms(OTP_AGE)

    await user.save()

    return res.json({
      status: 'ok'
    })
  } catch (error) {
    return next(error)
  }
}

const editProfile = async (req, res, next) => {
  try {
    const schemaParams = Joi.object().keys({
      id: Joi.number().required()
    })

    const { id } = await schemaParams.validateAsync(req.params)
    const body = await validateUser(req.body)

    body.phoneNumber = body.phoneNumber.replace(/^0/i, '')

    const user = await User.findByPk(id)

    if (!user) {
      throw createError(400, errMsg.USER_NOT_FOUND)
    }

    user.phoneNumber = body.phoneNumber
    user.bookName = body.bookName
    user.imageUrl = body.imageUrl
    user.name = body.name

    await user.save()

    let isPhoneNumberChanged = false
    if (user.phoneNumber !== body.phoneNumber) {
      if (SEND_OTP === 'true') {
        isPhoneNumberChanged = true
        const otp = generateOtp()

        user.otp = otp
        user.otpExpiredAt = Date.now() + ms(OTP_AGE)
        user.isActive = false

        user.save()

        await sendOtp(otp, user.phoneNumber)
      }
    }

    return res.json({
      status: 'ok',
      isPhoneNumberChanged,
      user: {
        name: user.name,
        bookName: user.bookName,
        phoneNumber: user.phoneNumber,
        countryCode: user.countryCode,
        imageUrl: user.imageUrl
      }
    })
  } catch (error) {
    return next(error)
  }
}

const deleteProfile = async (req, res, next) => {
  try {
    const schemaParams = Joi.object().keys({
      id: Joi.number().required()
    })

    const { id } = await schemaParams.validateAsync(req.params)

    await User.destroy({
      where: { id }
    })

    return res.json({
      status: 'ok'
    })
  } catch (error) {
    return next(error)
  }
}

module.exports = {
  login,
  checkOtp,
  resendOtp,
  editProfile,
  deleteProfile
}
