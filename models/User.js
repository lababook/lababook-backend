const { sequelize, Sequelize } = require('../libs/database')

const Hutang = require('./Hutang')
const Contact = require('./Contact')

class User extends Sequelize.Model {}

User.init({
  name: {
    type: Sequelize.STRING
  },
  bookName: {
    type: Sequelize.STRING
  },
  countryCode: {
    type: Sequelize.STRING,
    defaultValue: '+62'
  },
  phoneNumber: {
    type: Sequelize.STRING,
    unique: true
  },
  imageUrl: {
    type: Sequelize.STRING
  },
  otp: {
    type: Sequelize.STRING
  },
  otpExpiredAt: {
    type: Sequelize.DATE
  },
  isActive: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  password: {
    type: Sequelize.STRING
  }
}, {
  sequelize,
  paranoid: true,
  modelName: 'User'
})

User.hasMany(Hutang)
User.hasMany(Contact)

module.exports = User
