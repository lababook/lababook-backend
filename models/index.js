const db = require('../libs/database')

module.exports = {
  ...db,
  User: require('./User')
}
