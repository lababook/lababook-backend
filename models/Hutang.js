const { sequelize, Sequelize } = require('../libs/database')

const User = require('./User')
const Contact = require('./Contact')

class Hutang extends Sequelize.Model {}

Hutang.init({
  userId: {
    type: Sequelize.INTEGER,
    allowNull: false,
    references: {
      model: User
    }
  },
  contactId: {
    type: Sequelize.INTEGER,
    allowNull: false,
    references: {
      model: Contact
    }
  },
  delta: {
    type: Sequelize.BIGINT,
    allowNull: false
  },
  trxType: {
    type: Sequelize.STRING,
    enum: ['credit', 'debit']
  }
}, {
  sequelize,
  paranoid: true,
  tableName: 'Hutang',
  modelName: 'Hutang'
})

// Hutang.belongsTo(User)
// Hutang.belongsTo(Contact)

module.exports = Hutang
