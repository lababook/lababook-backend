const { sequelize, Sequelize } = require('../libs/database')

const User = require('./User')
// const Hutang = require('./Hutang')

class Contact extends Sequelize.Model {}

Contact.init({
  userId: {
    type: Sequelize.INTEGER,
    allowNull: false,
    references: {
      model: User
    }
  },
  name: {
    type: Sequelize.STRING
  },
  countryCode: {
    type: Sequelize.STRING,
    defaultValue: '+62'
  },
  phoneNumber: {
    type: Sequelize.STRING
  }
}, {
  sequelize,
  paranoid: true,
  modelName: 'Contact'
})

// Contact.belongsTo(User)
// Contact.hasMany(Hutang)

module.exports = Contact
