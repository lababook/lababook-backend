const authController = require('../controllers/auth')

const authenticate = require('../libs/middleware/authentication')
const { activeOnly } = require('../libs/middleware/authorization')

module.exports = router =>
  router
    .post('/login', authController.login)
    .post('/check-otp', authController.checkOtp)
    .post('/resend-otp', authController.resendOtp)

    .use(authenticate)
    .use(activeOnly)
    .put('/profile/edit/:id', authController.editProfile)
    .delete('/profile/delete/:id', authController.deleteProfile)
