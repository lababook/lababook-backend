const router = require('express').Router()

require('./auth')(router)
require('./hutang')(router)

module.exports = router
