const authenticate = require('../libs/middleware/authentication')
const { activeOnly } = require('../libs/middleware/authorization')

module.exports = router =>
  router
    .use(authenticate)
    .post(activeOnly)
    .post('/hutang/add', (req, res, next) => {})
    .post('/hutang/edit/:id', (req, res, next) => {})
    .post('/hutang/sync', (req, res, next) => {})
    .delete('/hutang/delete/:id', (req, res, next) => {})
    .get('/hutang/all', (req, res, next) => {})
    .get('/hutang/detail/:id', (req, res, next) => {})
