const http = require('http')
const { createTerminus } = require('@godaddy/terminus')

const app = require('../app')
const { sequelize } = require('../models')
const port = process.env.PORT || 3000
const server = http.createServer(app)

createTerminus(server, {
  onSignal: () => {
    console.log('closing database connection')

    return Promise.all([
      sequelize.close()
    ])
  }
})

server.listen(port, () => {
  console.log(
    `environment: ${process.env.NODE_ENV} \nrunning on http://localhost:${port}
    `
  )
})
